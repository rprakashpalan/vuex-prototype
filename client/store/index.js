import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// NOTES: Will likely want to break the store into modules as the application gets larger.
// SEE: https://vuex.vuejs.org/en/modules.html

// All mutations currently are _synchronous_ since there are no websocket calls.
// In order to do _asynchronous_ mutations, we will need to use "actions".

const state = {
  // The array of filters that are currently active:
  filters: [],

  filterHash: {},

  // The global dataset
  dataset: [],

  // user information
  user: {
    name: '',
    key: '',
    jwt: ''
  }
}

const mutations = {
  //--------------------------------------------------
  // ADDFILTER
  // payload --
  //    id          -- application identifier
  //    display     -- display
  //    placeholder -- placeholder name
  //    value       -- initial value of the filter (if any)
  ADDFILTER (state, payload) {
    console.log('Adding filter: ')
    console.info(payload)

    state.filters.push({
      id: payload.id,
      display: payload.display,
      placeholder: payload.placeholder,
      value: payload.value
    })

    console.log('Setting filterHash')
    Vue.set(state.filterHash, payload.id, payload)
    console.info(state.filterHash)
  },

  //--------------------------------------------------
  // REMOVEFILTER
  REMOVEFILTER (state, payload) {
    // TODO -- this does not remove the filter from the filterHash (yet)!
    let filterIdex = state.filters.findIndex(item => {
      return (item.id == payload.id)
    })

    if (filterIndex === -1) {
      console.warn('Can not remove filter element, it was not found.')
      return
    }

    state.filters = state.filters.splice(filterIndex, 1)
  },

  //--------------------------------------------------
  // REMOVEALLFILTERS
  REMOVEALLFILTERS(state) {
    state.filters = []
    state.filterHash = {}
  },

  //--------------------------------------------------
  // SETFILTERVALUE
  // payload --
  //   id       -- id of the filter
  //   value    -- filter value
  SETFILTERVALUE(state, payload) {
    Vue.set(state.filterHash[payload.id], 'value', payload.value)
  },

  //--------------------------------------------------
  // DATAINIT
  DATAINIT(state, payload) {
    state.dataset = payload
  }
}

const actions = {
  // incrementAsync ({ commit }) {
  //   setTimeout(() => {
  //     commit('INCREMENT')
  //   }, 200)
  // }
}

const getters = {
  //-----------------------------------------------------------------
  // filteredViewData is the data as it exists _post filtration_
  // If there are no active filters, it returns the original dataset.
  filteredViewData: state => {
    let filterHash = state.filterHash
    let filters = state.filters
    let dataset = state.dataset

    let isFiltered = () => {
      if (filters.length === 0) return false;
      return filters.some (filter => filter.value !== '')
    }

    if (!isFiltered()) {
      return state.dataset
    }

    return dataset.
      filter( item => { 
        return item.name.includes(filterHash['name-filter'].value)
      })
      .filter( item => {
        return filterHash['id-filter'].value === '' || item.id == filterHash['id-filter'].value
      })
  },

  //-----------------------------------------------------------------
  // user is the user object. It will pull from persistence if the object
  // is not currently in the store.
  user: state => {
    if (state.user.username === '' || state.user.key === '') {
      store.commit('SETUSERNAME', {username: un})
      store.commit('SETUSERKEY', {key: key})
    }

    return state.user
  }  
}

const store = new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})

export default store
